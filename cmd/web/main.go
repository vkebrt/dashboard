package main

import (
	"log"
	"net/http"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", homePageFunc)
	mux.HandleFunc("/record", recordsPageFunc)
	mux.HandleFunc("/record/create", recordCreateFunc)

	log.Printf("[Info] Starting server on :4000/tcp")
	err := http.ListenAndServe(":4000", mux)
	if err != nil {
		log.Fatalln(err)
	}
}
